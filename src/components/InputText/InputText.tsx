import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
} from "@chakra-ui/react";
import { FieldErrors } from "react-hook-form";

export const InputText = (props: {
  errors: FieldErrors;
  label: string;
  name: string;
  register: Function;
  requiredText: string;
}) => {
  return (
    <FormControl isInvalid={props.errors[props.name]}>
      <FormLabel htmlFor={props.name}>{props.label}</FormLabel>
      <Input
        type="text"
        {...props.register(props.name, {
          required: props.requiredText,
        })}
      />
      <FormErrorMessage>
        {props.errors[props.name] && props.errors[props.name].message}
      </FormErrorMessage>
    </FormControl>
  );
};
