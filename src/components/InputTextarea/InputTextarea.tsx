import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Textarea,
} from "@chakra-ui/react";
import { FieldErrors } from "react-hook-form";

export const InputTextarea = (props: {
  errors: FieldErrors;
  label: string;
  name: string;
  placeholder: string;
  register: Function;
  requiredText: string;
}) => (
  <>
    <FormControl isInvalid={props.errors[props.name]}>
      <FormLabel htmlFor={props.name}>{props.label}</FormLabel>
      <Textarea
        name={props.name}
        placeholder={props.placeholder}
        {...props.register(props.name, {
          required: props.requiredText,
        })}
      />
      <FormErrorMessage>
        {props.errors[props.name] && props.errors[props.name].message}
      </FormErrorMessage>
    </FormControl>
  </>
);
