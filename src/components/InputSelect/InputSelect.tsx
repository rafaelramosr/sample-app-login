import { FormControl, FormErrorMessage, FormLabel } from "@chakra-ui/react";
import { OptionBase, Select } from "chakra-react-select";
import { Controller, FieldErrors } from "react-hook-form";

interface CommonSelection extends OptionBase {
  label: string;
  value: string | number;
}

export const InputSelect = (props: {
  control: any;
  errors: FieldErrors;
  label: string;
  name: string;
  requiredText: string;
  options: CommonSelection[];
}) => {
  return (
    <Controller
      control={props.control}
      name={props.name}
      rules={{ required: props.requiredText }}
      render={({ field: { onChange, onBlur, value, ref } }) => (
        <FormControl id={props.name} isInvalid={props.errors[props.name]}>
          <FormLabel>{props.label}</FormLabel>
          <Select<CommonSelection, false>
            name={props.name}
            onBlur={onBlur}
            onChange={onChange}
            options={props.options}
            placeholder="Seleccione una opción..."
            ref={ref}
            size="sm"
            value={value}
            chakraStyles={{
              dropdownIndicator: (provided) => ({
                ...provided,
                bg: "transparent",
                px: 2,
                cursor: "inherit",
              }),
              indicatorSeparator: (provided) => ({
                ...provided,
                display: "none",
              }),
            }}
          />
          <FormErrorMessage>
            {props.errors[props.name] && props.errors[props.name].message}
          </FormErrorMessage>
        </FormControl>
      )}
    />
  );
};
