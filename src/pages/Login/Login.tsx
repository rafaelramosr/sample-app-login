import {
  Box,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { ColorModeSwitcher, Logo } from "../../components";
import "./login.css";

export default function Login() {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();

  return (
    <Box textAlign="right" fontSize="xl">
      <ColorModeSwitcher justifySelf="flex-end" />

      <section className="login">
        <form className="login__form" onSubmit={handleSubmit(() => {})}>
          <Logo h="2em" pointerEvents="none" />
          <FormControl isInvalid={errors.email}>
            <FormLabel htmlFor="username">Nombre de usuario</FormLabel>
            <Input
              type="text"
              {...register("username", {
                required: "Nombre de usuario es requerido.",
              })}
            />
            <FormErrorMessage>
              {errors.username && errors.username.message}
            </FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={errors.password}>
            <FormLabel htmlFor="password">Contraseña</FormLabel>
            <Input
              type="text"
              {...register("password", {
                required: "La contraseña es requerida.",
              })}
            />
            <FormErrorMessage>
              {errors.password && errors.password.message}
            </FormErrorMessage>
          </FormControl>
          <Button
            mt={4}
            colorScheme="blue"
            isLoading={isSubmitting}
            type="submit"
          >
            Iniciar sesión
          </Button>
        </form>
      </section>
    </Box>
  );
}
