import { useForm } from "react-hook-form";
import { Button } from "@chakra-ui/react";
import { InputSelect, InputText, InputTextarea } from "../../components";

export default function Cases() {
  const {
    control,
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();

  function onSubmit(values: any) {
    return new Promise((resolve) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        resolve(true);
      }, 1000);
    });
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <InputText
        errors={errors}
        label="Input de texto ejemplo"
        name="test"
        register={register}
        requiredText="Este campo es requerido."
      />
      <InputSelect
        control={control}
        errors={errors}
        label="Selector ejemplo"
        name="selector-1"
        requiredText="El selector 1 es obligatorio."
        options={[
          { label: "uno", value: 1 },
          { label: "dos", value: 2 },
        ]}
      />

      <InputTextarea
        errors={errors}
        label="Textarea ejemplo"
        name="textarea-1"
        placeholder="Escriba un mensaje"
        register={register}
        requiredText="EL textarea es obligatorio"
      />
      <Button mt={4} colorScheme="blue" isLoading={isSubmitting} type="submit">
        Submit
      </Button>
    </form>
  );
}
