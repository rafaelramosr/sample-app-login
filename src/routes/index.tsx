import { ChakraProvider, theme } from "@chakra-ui/react";
import { Route, Switch } from "wouter";
import { Cases, Login } from "../pages";

export default function App() {
  return (
    <ChakraProvider theme={theme}>
      <Switch>
        <Route path="/" component={Login} />
        <Route path="/cases" component={Cases} />
      </Switch>
    </ChakraProvider>
  );
}
